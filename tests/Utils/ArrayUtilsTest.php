<?php

namespace Compass\UrlTokenBundle\Tests\Utils;

use Compass\UrlTokenBundle\Utils\ArrayUtils;
use PHPUnit\Framework\Attributes\CoversNothing;
use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\TestCase;

#[CoversNothing]
class ArrayUtilsTest extends TestCase
{
    #[DataProvider('provideRemoveData')]
    public function testRemove(array $expected, array $array, int|string|array $key): void
    {
        ArrayUtils::remove($array, $key);

        $this->assertEquals($expected, $array);
    }

    public static function provideRemoveData(): \Generator
    {
        yield [
            ['bar' => 'baz'],
            ['foo' => 'bar', 'bar' => 'baz'],
            'foo'
        ];
        yield [
            [],
            ['foo' => 'bar', 'bar' => 'baz'],
            ['foo', 'bar']
        ];
        yield [
            [],
            [],
            'foo'
        ];
        yield [
            [],
            [],
            ['foo', 'bar']
        ];
    }

    #[DataProvider('providePullData')]
    public function testPull(mixed $expected, array $array, int|string $key, array $after): void
    {
        $this->assertEquals($expected, ArrayUtils::pull($array, $key));
        $this->assertEquals($after, $array);
    }

    public static function providePullData(): \Generator
    {
        yield [
            'bar',
            ['foo' => 'bar'],
            'foo',
            [],
        ];
        yield [
            null,
            [],
            'foo',
            [],
        ];
        yield [
            null,
            ['foo' => 'bar'],
            'baz',
            ['foo' => 'bar'],
        ];
    }

    #[DataProvider('provideGetData')]
    public function testGet(mixed $expected, array $array, int|string $key): void
    {
        $this->assertEquals($expected, ArrayUtils::get($array, $key));
    }

    public static function provideGetData(): \Generator
    {
        yield [
            'bar',
            ['foo' => 'bar'],
            'foo',
        ];
        yield [
            null,
            ['foo' => 'bar'],
            'baz'
        ];
    }
}
