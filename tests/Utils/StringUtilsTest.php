<?php

namespace Compass\UrlTokenBundle\Tests\Utils;

use Compass\UrlTokenBundle\Utils\StringUtils;
use PHPUnit\Framework\Attributes\CoversNothing;
use PHPUnit\Framework\TestCase;

#[CoversNothing]
class StringUtilsTest extends TestCase
{

    public function testBefore()
    {
        $this->assertEquals("http", StringUtils::before('http://example.com/', ':'));
    }

    public function testBeforeNotContainCharReturnsEmptyString()
    {
        $this->assertEquals('', StringUtils::before('http://example.com/', '@'));
    }

    public function testAfter()
    {
        $this->assertEquals('//example.com/', StringUtils::after('http://example.com/', ':'));
    }

    public function testAfterNotContainsCharReturnsEmptyString()
    {
        $this->assertEquals('', StringUtils::after('http://example.com/', '@'));
    }
}
