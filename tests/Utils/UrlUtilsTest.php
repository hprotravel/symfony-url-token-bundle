<?php

namespace Compass\UrlTokenBundle\Tests\Utils;

use Compass\UrlTokenBundle\Utils\UrlUtils;
use PHPUnit\Framework\Attributes\CoversNothing;
use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\TestCase;

#[CoversNothing]
class UrlUtilsTest extends TestCase
{

    #[DataProvider('provideGetBaseUrlData')]
    public function testGetBaseUrl(string $expected, string $url): void
    {
        $this->assertEquals(
            $expected,
            UrlUtils::getBaseUrl($url)
        );
    }

    public static function provideGetBaseUrlData(): \Generator
    {
        yield [
            'http://web.compass.develop/create-password',
            'http://web.compass.develop/create-password?userId=10&tokenType=activation',
        ];
        yield [
            'http://web.compass.develop/create-password',
            'http://web.compass.develop/create-password',
        ];
    }

    #[DataProvider('provideGetQueryParamsData')]
    public function testGetQueryParams(array $expected, string $url)
    {
        $this->assertEquals($expected, UrlUtils::getQueryParams($url));
    }

    public static function provideGetQueryParamsData(): \Generator
    {
        yield [
            [
                'userId' => '10',
                'tokenType' => 'activation'
            ],
            'http://web.compass.develop/create-password?userId=10&tokenType=activation'
        ];
        yield [
            [
                'userId' => '10',
                'tokenType' => 'activation'
            ],
            'http://web.compass.develop/create-password?tokenType=activation&userId=10'
        ];
    }

    #[DataProvider('provideAddQueryParamsData')]
    public function testAddQueryParams(string $expected, string $url, array $params)
    {
        $this->assertEquals($expected, UrlUtils::addQueryParams($url, $params));
    }

    public static function provideAddQueryParamsData(): \Generator
    {
        yield [
            'http://web.compass.develop/create-password?tokenType=activation&userId=10&expires=1234567890&token=5b0af497a08a4ca46f401553753c1b02e12479bfce23fe100e1ee44ef25d9d55',
            'http://web.compass.develop/create-password?tokenType=activation&userId=10',
            ['expires' => 1234567890, 'token' => '5b0af497a08a4ca46f401553753c1b02e12479bfce23fe100e1ee44ef25d9d55']
        ];
    }
}
