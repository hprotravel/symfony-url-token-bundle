<?php

namespace Compass\UrlTokenBundle\Tests;

use Compass\UrlTokenBundle\UrlTokenGenerator;
use Compass\UrlTokenBundle\UrlTokenGeneratorInterface;
use PHPUnit\Framework\Attributes\CoversNothing;
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Cache\Adapter\ArrayAdapter;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;

#[CoversNothing]
class UrlTokenGeneratorTest extends TestCase
{

    private static UrlTokenGeneratorInterface $urlTokenGenerator;

    public static function setUpBeforeClass(): void
    {
        self::$urlTokenGenerator = new UrlTokenGenerator('secret');
    }

    public function testGenerateToken()
    {
        $token = self::$urlTokenGenerator->generateToken('https://example.com', 300);

        $this->assertNotEmpty($token);
        $this->assertEquals(64, strlen($token));

        return $token;
    }

    public function testValidateToken()
    {
        $token = '5b0af497a08a4ca46f401553753c1b02e12479bfce23fe100e1ee44ef25d9d55';

        $this->assertTrue(self::$urlTokenGenerator->validateToken('https://example.com', 300, $token));
    }

    public function testGenerateUrl(): string
    {
        $url = self::$urlTokenGenerator->generateUrl('https://example.com?userId=1&status=active', 300);

        $pattern = '~^https://example\.com\?status=[a-zA-Z]+&userId=\d+&expires=\d+&token=[a-fA-F0-9]+$~';

        $this->assertMatchesRegularExpression($pattern, $url);

        return $url;
    }

    public function testGenerateUrlWithBaseUrlEndingTrailingSlash()
    {
        $url = self::$urlTokenGenerator->generateUrl('https://example.com/?userId=1&status=active', 300);

        $pattern = '~^https://example\.com/\?status=[a-zA-Z]+&userId=\d+&expires=\d+&token=[a-fA-F0-9]+$~';

        $this->assertMatchesRegularExpression($pattern, $url);
    }

    #[Depends('testGenerateUrl')]
    public function testValidateUrl(string $url): void
    {
        $this->assertTrue(self::$urlTokenGenerator->validateUrl($url));
    }

    public function testValidateUrlReturnsFalseWhenExpiresMissing()
    {
        $url = 'https://example.com/?userId=1&status=active&token=b58621c7c3f8687667815e14296c7dc748379872d83815d57f6ed9d0d978e625';

        $this->assertFalse(self::$urlTokenGenerator->validateUrl($url));
    }

    public function testValidateUrlReturnsFalseWhenExpiresWrong()
    {
        $url = 'https://example.com/?userId=1&status=active&expires=1718984110&token=b58621c7c3f8687667815e14296c7dc748379872d83815d57f6ed9d0d978e625';

        $this->assertFalse(self::$urlTokenGenerator->validateUrl($url));
    }

    public function testValidateUrlReturnsFalseWhenTokenMissing()
    {
        $url = 'https://example.com/?userId=1&status=active&expires=1718984111';

        $this->assertFalse(self::$urlTokenGenerator->validateUrl($url));
    }

    public function testValidateUrlReturnsFalseWhenTokenWrong()
    {
        $url = 'https://example.com/?userId=1&status=active&expires=1718984111&token=b58621c7c3f8687667815e14296c7dc748379872d83815d57f6ed9d0d978e627';

        $this->assertFalse(self::$urlTokenGenerator->validateUrl($url));
    }

    public function testValidateUrlReturnsFalseWhenExpired()
    {
        $url = self::$urlTokenGenerator->generateUrl('https://example.com/?userId=1&status=active', 0);

        \sleep(1);

        $this->assertFalse(self::$urlTokenGenerator->validateUrl($url));
    }

    public function testValidateUrlReturnsFalseWhenNotExpired()
    {
        $url = self::$urlTokenGenerator->generateUrl('https://example.com/?userId=1&status=active', 2);

        \sleep(1);

        $this->assertTrue(self::$urlTokenGenerator->validateUrl($url));
    }

    public function testValidateUnsortedData()
    {
        $this->assertEquals(
            self::$urlTokenGenerator->generateUrl(
                'http://web.compass.develop/create-password?userId=10&tokenType=activation',
                300
            ),
            self::$urlTokenGenerator->generateUrl(
                'http://web.compass.develop/create-password?tokenType=activation&userId=10',
                300
            )
        );
    }

    public function testGenerateUrlWithCache()
    {
        $cache = new ArrayAdapter();

        $urlTokenGenerator = new UrlTokenGenerator('secret', cache: $cache);

        $url = $urlTokenGenerator->generateUrl('https://example.com?userId=1&status=active', 300);

        $pattern = '~^https://example\.com\?status=[a-zA-Z]+&userId=\d+&expires=\d+&token=[a-fA-F0-9]+$~';

        $this->assertMatchesRegularExpression($pattern, $url);

        $this->assertTrue($urlTokenGenerator->validateUrl($url));

        $urlTokenGenerator->revokeUrlCache($url);

        $this->assertFalse($urlTokenGenerator->validateUrl($url, verifyCache: true));

        // Wait for getting a fresh token
        \sleep(1);

        $urlTokenGenerator->generateUrl('https://example.com?userId=1&status=active', 300);

        $this->assertFalse($urlTokenGenerator->validateUrl($url, verifyCache: true));

        return $url;
    }

    #[Depends('testGenerateUrlWithCache')]
    public function testGenerateUrlWithInExpireTime(string $url): void
    {
        $this->assertTrue(true);
    }

    public function testGenerateUrlWithVerify(): void
    {
        $url = self::$urlTokenGenerator->generateUrl(
            'http://web.compass.develop/create-password?userId=10&tokenType=activation',
            300
        );

        $this->assertTrue(self::$urlTokenGenerator->validateUrl($url, ['userId' => 10]));
        $this->assertTrue(self::$urlTokenGenerator->validateUrl($url, ['userId' => '10']));
        $this->assertFalse(self::$urlTokenGenerator->validateUrl($url, ['userId' => 100]));
    }

    public function testValidateCurrentUrl()
    {
        $url = self::$urlTokenGenerator->generateUrl(
            'http://web.compass.develop/create-password?userId=10&tokenType=activation',
            300
        );

        $request = Request::create($url);

        $requestStack = new RequestStack();
        $requestStack->push($request);

        self::$urlTokenGenerator->setRequestStack($requestStack);

        $this->assertTrue(self::$urlTokenGenerator->validateCurrentUrl());
    }

    public function testValidateCurrentUrlThrowsExceptionWithoutCurrentRequest(): void
    {
        $this->expectException(\RuntimeException::class);
        $this->expectExceptionMessage('Current request stack is not set.');

        self::$urlTokenGenerator->generateUrl(
            'http://web.compass.develop/create-password?userId=10&tokenType=activation',
            300
        );

        $requestStack = new RequestStack();

        self::$urlTokenGenerator->setRequestStack($requestStack);

        self::$urlTokenGenerator->validateCurrentUrl();
    }
}
