## 0.4.1 (August 01, 2024)
  - Hotfix - CK-643 - Add optional verifyCache param to fix unwanted cache validate issue

## 0.4.0 (July 12, 2024)
  - Implement new UrlTokenGeneratorInterface::validateCurrentUrl method

## 0.3.3 (July 11, 2024)
  - Implement verify parameter to UrlTokenGeneratorInterface::validateUrl method
  - Add verify parameter to UrlTokenGeneratorInterface::validateUrl method
  - Refactor test for ArrayUtils::pull and add ArrayUtils::get method
  - Fix getting base url without query params

## 0.3.2 (July 08, 2024)
  - Fix generating duplicated cache key from same url

## 0.3.1 (June 24, 2024)
  - Add to gitignore composer.lock file

## 0.3.0 (June 24, 2024)
  - Fix namespace

## 0.2.0 (June 24, 2024)
  - Implement cache
  - Add bump-version bash script

## 0.1.0 (June 23, 2024)
  - Add README file
  - Remove .idea folder from git
  - Add reusable bundle config
  - Finish implementation
  - Extract method getIntendedUrl
  - Fix test method testValidateUrl fails
  - Remove and ignore modified files
  - Add UrlTokenGenerator.php implementation
  - Add utils
  - Add ignore directory and files for ide and test suite
  - Install phpunit/phpunit
  - Add UrlTokenGeneratorInterface.php
  - Initial commit

