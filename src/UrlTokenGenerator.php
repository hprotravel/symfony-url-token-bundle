<?php

namespace Compass\UrlTokenBundle;

use Compass\UrlTokenBundle\Utils\ArrayUtils;
use Compass\UrlTokenBundle\Utils\UrlUtils;
use DateTimeImmutable;
use Psr\Cache\CacheItemInterface;
use Psr\Cache\CacheItemPoolInterface;
use Symfony\Component\HttpFoundation\RequestStack;

class UrlTokenGenerator implements UrlTokenGeneratorInterface
{
    private RequestStack $requestStack;

    public function __construct(
        private readonly string $secret,
        private readonly string $algo = 'sha256',
        private readonly ?CacheItemPoolInterface $cache = null,
    ) {
    }

    public function setRequestStack(RequestStack $requestStack): void
    {
        $this->requestStack = $requestStack;
    }

    #[\Override] public function generateToken(string $data, int $expiration): string
    {
        return \hash_hmac($this->algo, "$data::$expiration", $this->secret);
    }

    #[\Override] public function validateToken(string $data, int $expiration, string $token): bool
    {
        return \hash_equals($this->generateToken($data, $expiration), $token);
    }

    #[\Override] public function generateUrl(string $url, int $expiration): string
    {
        $expires = $this->getExpireTimestamp($expiration);

        $intendedUrl = $this->getIntendedUrl($url);

        $token = $this->generateToken($intendedUrl, $expires);

        if ($this->cache) {
            $this->cache->get(
                $token,
                function (CacheItemInterface $item) use ($expiration, $token) {
                    $item->expiresAfter($expiration);

                    return $token;
                }
            );
        }

        return UrlUtils::addQueryParams($intendedUrl, ['expires' => $expires, 'token' => $token]);
    }

    #[\Override] public function validateUrl(string $url, array $verify = [], bool $verifyCache = false): bool
    {
        $queryParams = UrlUtils::getQueryParams($url);

        $expires = ArrayUtils::pull($queryParams, 'expires');
        $token = ArrayUtils::pull($queryParams, 'token');

        if ($expires === null || $token === null) {
            return false;
        }

        if (!$this->validateExpires($expires)) {
            return false;
        }

        $intendedUrl = $this->getIntendedUrl($url);

        if (!$this->validateToken($intendedUrl, $expires, $token)) {
            return false;
        }

        if ([] !== $verify) {
            foreach ($verify as $key => $value) {
                if (ArrayUtils::get($queryParams, $key) != $value) {
                    return false;
                }
            }
        }

        if ($this->cache && $verifyCache) {
            $item = $this->cache->getItem($token);

            if (!$item->isHit()) {
                return false;
            }
        }

        return true;
    }

    public function validateCurrentUrl(array $verify = [], bool $verifyCache = false): bool
    {
        if (null === $this->requestStack?->getCurrentRequest()) {
            throw new \RuntimeException('Current request stack is not set.');
        }

        $url = $this->requestStack->getCurrentRequest()->getUri();

        return $this->validateUrl($url, $verify, $verifyCache);
    }

    #[\Override] public function revokeUrlCache(string $url): void
    {
        if ($this->cache) {
            $queryParams = UrlUtils::getQueryParams($url);

            $token = ArrayUtils::pull($queryParams, 'token');

            $this->cache->deleteItem($token);
        }
    }

    private function getExpireTimestamp(int $expiresInSeconds): int
    {
        return time() + $expiresInSeconds;
    }

    private function validateExpires(int $expires): bool
    {
        return $expires >= (new DateTimeImmutable())->getTimestamp();
    }

    private function getIntendedUrl(string $url): string
    {
        $baseUrl = UrlUtils::getBaseUrl($url);
        $queryParameters = UrlUtils::getQueryParams($url);

        ArrayUtils::remove($queryParameters, ['expires', 'token']);

        return $baseUrl . '?' . \http_build_query($queryParameters);
    }
}