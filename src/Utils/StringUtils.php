<?php

namespace Compass\UrlTokenBundle\Utils;

class StringUtils
{
    public static function before(string $string, string $char): string
    {
        return \substr($string, 0, \strpos($string, $char));
    }

    public static function after(string $string, string $char): string
    {
        if (!\str_contains($string, $char)) {
            return '';
        }

        return \substr($string, \strpos($string, $char) + 1);
    }
}