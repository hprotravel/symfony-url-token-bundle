<?php

namespace Compass\UrlTokenBundle\Utils;

class UrlUtils
{

    public static function getBaseUrl(string $url): string
    {
        return StringUtils::before($url, '?') ?: $url;
    }

    public static function getQueryParams(string $url): array
    {
        $queryString = StringUtils::after($url, '?');

        $queryParams = [];
        if ($queryString !== '') {
            \parse_str($queryString, $queryParams);
        }

        \ksort($queryParams);

        return $queryParams;
    }

    public static function addQueryParams(string $url, array $queryParams): string
    {
        $urlQueryParams = self::getQueryParams($url);

        $allQueryParams = array_merge($urlQueryParams, $queryParams);

        $baseUrl = self::getBaseUrl($url);

        return $baseUrl . '?' . http_build_query($allQueryParams);
    }
}