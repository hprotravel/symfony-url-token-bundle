<?php

namespace Compass\UrlTokenBundle\Utils;

class ArrayUtils
{
    public static function remove(array &$array, int|string|array $key): void
    {
        foreach ((array) $key as $k) {
            unset($array[$k]);
        }
    }

    public static function pull(array &$array, mixed $key): mixed
    {
        if (\array_key_exists($key, $array)) {
            $value = $array[$key];

            unset($array[$key]);

            return $value;
        }

        return null;
    }

    public static function get(array $array, string $string): mixed
    {
        return $array[$string] ?? null;
    }
}

?>
