<?php

namespace Compass\UrlTokenBundle;

interface UrlTokenGeneratorInterface
{
    public function generateToken(string $data, int $expiration): string;

    public function validateToken(string $data, int $expiration, string $token): bool;

    public function generateUrl(string $url, int $expiration): string;

    public function validateUrl(string $url, array $verify = [], bool $verifyCache = false): bool;

    public function validateCurrentUrl(array $verify = [], bool $verifyCache = false): bool;

    public function revokeUrlCache(string $url): void;
}